### SQL
Dans un fichier blissim.sql, construire une architecture de base de données pour un site
E-commerce. L'objectif est de pouvoir stocker, en SQL, les commandes, ainsi que leurs
détails dans une architecture logique et performante. Une fois que celle-ci est écrite, remplir les tables avec quelques commandes test.

>fichier blissim.sql à la racine du dépôt

Ensuite, écrivez 2 requêtes SQL avec le comportement suivant :
1. Récupérer le prénom et le nom de famille de tous les clients qui ont commandé le
produit avec l’ID 1;
2. Récupérer tous les noms et quantités des produits vendus sur les 7 derniers jours.

>fichier test_requests.sql à la racine du dépôt

### PHP
Créer une application en PHP qui :
1. Récupère une liste de produits par API à l'adresse suivante
https://fakestoreapi.com/products
2. Affiche ces produits (titre, image, prix)
3. Permet d'ajouter, supprimer et modifier un commentaire sur n'importe quel produit
via un formulaire HTML. Le stockage de ces commentaires se fait soit en session,
soit en base de donnée via PDO

> un fichier comment.sql est fournis pour la structure de la table
> la config sql se passe dans Blissim\Models\DbFactory
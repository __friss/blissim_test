<?php
namespace Blissim\Models;
use Blissim\Controllers\FrontController;
class Comment
{
	private $_data = null;

	
	///private static $_userList = null;

	public function __construct($data)
	{
		$this->_data = $data;
	}

	public static function validate(array $data){
		//array('name_comment'=>'required','email_comment'=>'required|email',)
		$errors = array();
		foreach ($data as $key => &$value) {
			$value = trim($value);
			if($key=='name_comment') {
				if(strlen($value)<1){
					$errors[$key] = 'Name cannot be empty';
				}elseif (strlen($value)>60) {
					$errors[$key] = 'Name cannot exceed 60 chars';
				}
			
			}elseif($key=='email_comment' && false === filter_var($value,FILTER_VALIDATE_EMAIL))
			{
				$errors[$key] = 'The email is invalid';

			}elseif($key=='text_comment' && strlen($value)<1){
				$errors[$key] = 'Comment cannot be empty';
			}
		}
		return (empty($errors)) ? $data : array('errors'=>$errors);
	}

	public function getData($data)
	{
		if(is_array($this->_data) && isset($this->_data[$data]))
		{
			return $this->_data[$data];
		}
		return null;
	}

	public static function find($id){
		$id = intval($id);
		if($id<1) return null;
		$db = FrontController::getInstance()->getDb();
		$sql = "SELECT * FROM comments WHERE id_comment = :id";
		$query = $db->prepare($sql);
		$query->bindValue(':id',$id,\PDO::PARAM_INT);
		$query->execute();
		return $query->fetch();
	} 

	public static function destroy($id){
		$id = intval($id);
		if($id<1) return null;
		$db = FrontController::getInstance()->getDb();
		$sql = "DELETE FROM comments WHERE id_comment = :id";
		$query = $db->prepare($sql);
		$query->bindValue(':id',$id,\PDO::PARAM_INT);
		return $query->execute();
	}

	public static function product($id){
		$id = intval($id);
		if($id<1) return null;
		$db = FrontController::getInstance()->getDb();
		$sql = "SELECT * FROM comments WHERE id_product = :id";
		$query = $db->prepare($sql);
		$query->bindValue(':id',$id,\PDO::PARAM_INT);
		$query->execute();
		//var_dump($query);
		return $query->fetchAll();
	} 

	public static function count($product_id){
		$product_id = intval($product_id);
		if($product_id<1) return null;
		$db = FrontController::getInstance()->getDb();
		$sql = "SELECT COUNT(id_comment) FROM comments WHERE id_product = :id";
		$query = $db->prepare($sql);
		$query->bindValue(':id',$product_id,\PDO::PARAM_INT);
		$query->execute();
		//var_dump($query);
		return $query->fetchColumn();
	}

	public static function create(array $payload){
		$db = FrontController::getInstance()->getDb();
		$sql = "INSERT INTO comments SET name_comment = :name_comment, email_comment = :email_comment, text_comment=:text_comment, id_product=:id_product, created_at=:created_at, updated_at=:updated_at";
		$query = $db->prepare( $sql );

		if(true === $query->execute($payload)){
			return $db->lastInsertId();
		}
		return false;

	}

	public static function update(array $payload){
		$db = FrontController::getInstance()->getDb();
		$sql = "UPDATE comments SET name_comment = :name_comment, email_comment = :email_comment, text_comment = :text_comment, id_comment = :id_comment, updated_at = :updated_at WHERE id_comment = :id_comment";
		$query = $db->prepare( $sql );

		return $query->execute($payload);

	}


}
<?php 
namespace Blissim\Models;
class DBFactory
{

	private static $db;
	private static $debug = false; 
	private static $config = array('host'=>'localhost', 'dbname'=>'blissim', 'user'=>'root', 'password'=>'');

	public static function getPDOConnexion()
	{
		$config = self::$config;

		if(static::$db===null)
		{				
			try{
            	$db = new \PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'], $config['user'], $config['password']);//array(PDO::MYSQL_SET_ATTR_INIT_COMMAND=>'SET NAMES utf8'));
				//$db->exec("SET CHARACTER SET utf8");
				$db->exec('SET NAMES "UTF8"');
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); //exception
				$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
			}catch(PDOException $e){

				if(true === self::$debug)
					die($e->getMessage());
				else
					die(utf8_encode('Base de donnée indisponible'));					
			}

			static::$db = $db;
		}

		return static::$db;			
	}


	// public static function getMysqliConnexion()
	// {
	// 	$config = self::$config;

	// 	if(static::$db===null)
	// 	{
	// 		//msqli co
	// 		$db = new \mysqli($config['host'], $config['user'], $config['password'], $config['dbname']);
	// 	}

	// 	return static::$db;	

	// }
}
	
?>
<?php
namespace Blissim\Models;
class User
{
	private $_data = null;
	private static $_userList = null;

	public function __construct($data)
	{
		$this->_data = $data;
	}

	public function getData($data)
	{
		if(is_array($this->_data) && isset($this->_data[$data]))
		{
			return $this->_data[$data];
		}
		return null;
	}

	public static function getUserList()
	{
		if(is_null(self::$_userList))
		{
			$db = FrontController::getInstance()->getDb();
			$sql = "SELECT id, name FROM users";
			$req = $db->query($sql);
			$results = $req->fetchAll(PDO::FETCH_ASSOC);
			self::$_userList = array();
			if(is_array($results))
			{
				foreach($results as $result)
				{
					self::$_userList[] = new self($result);
				}
			}
		}
		return self::$_userList;
	}
}
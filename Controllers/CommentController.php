<?php
namespace Blissim\Controllers;

use Blissim\Models\Comment;
use Blissim\Lib\Formchecker;
class CommentController extends AbstractController{

	public function editAction()
	{
		$id = $this->getFc()->getParam('id');
		if(intval($id)<1){
			header('Location:/404');
			exit();
		}

		$comment = Comment::find($id);
		
		$action = $this->getFc()->action;
		$controller = $this->getFc()->controller;
		$this->getFc()->setViewParams('comment',$comment );
		
	}

	public function deleteAction(){
		
		$this->getFc()->checkMandatory($_POST,array('csrf'));

		$id = $this->getFc()->getParam('id');
		if(intval($id)<1){
			header('Location:/404');
			exit();
		}

		$comment = Comment::find($id);

		$formChecker = new Formchecker();
		
		$formChecker->setAllowedReferer( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://'.$_SERVER['SERVER_NAME'].'/product/showcomments/'.$comment->id_product);

		if($formChecker->isClean())
        {
        	if(Comment::destroy($id)){
	            			$msg = 'Comment successfully destroyed';
	            		}else{
	            			$msg = 'Database error, your comment has not been deleted';
	            		}


        }else{
        	$msg ='An error occured<br>';
	        $msg = $formChecker->getErrors();
        }

        $this->getFc()->setFlash($msg);
			header('Location: /product/showcomments/'.$comment->id_product);
			exit();
	}


	public function updateAction(){

		$this->getFc()->checkMandatory($_POST,array('csrf'));

		$id = $this->getFc()->getParam('id');
		if(intval($id)<1){
			header('Location:/404');
			exit();
		}

		$msg='';
		
			$formChecker = new Formchecker();//::get_instance();
			$formChecker->setAllowedReferer( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://'.$_SERVER['SERVER_NAME'].'/comment/edit/'.$id);
			if($formChecker->isClean())
            {
            	$payload = Comment::validate($_POST);
            	if(!isset($payload['errors'])){
            		if(isset($payload['csrf'])){
	            		unset($payload['csrf']);
	            	}
	            	$payload['id_comment'] = $id;
            		$payload['updated_at'] = date('Y-m-d H:i:s');

            		if(Comment::update($payload)){
	            			$msg = 'Comment successfully updated';
	            		}else{
	            			$msg = 'Database error, your comment has not been updated';
	            		}
            	}else{
            		$msg ='An error occured<br>';
	            	$msg .= implode('<br>',$payload['errors']);
            	}
            }else{
            	$msg ='An error occured<br>';
	            $msg = $formChecker->getErrors();
            }
            $this->getFc()->setFlash($msg);
			header('Location: /comment/edit/'.$id);
			exit();

		
	}

}
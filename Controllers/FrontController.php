<?php
namespace Blissim\Controllers;
use PDO;
use Blissim\Lib\Request;
class FrontController{

	private $controller = 'default';
	private $action = 'default';
	private $params = array();
	//private $vars = array();
	private $db = null;
	protected $request;
	protected $app;
	private static $instance = null;

		protected $viewParams = array();

	private function __construct()
	{
		//$this->app = $this;
	}

	private function __clone()
	{

	}

	public static function getInstance()
	{
		if(is_null(self::$instance))
		{
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function checkMandatory(array $array, array $keys){

		$valid = true;
		if(!empty($keys)){
			foreach ($keys as $key) {
				if(!isset($array[$key])){
					$valid = false;
					break;
				}
			}
		}
		if(false === $valid){
			$this->abort(403);
		}
	}

	public function run()
	{
		$this->getHeader();

		$this->route();
		$this->request = new Request();

		
//var_dump($this->controller);
		$controllerName= 'Blissim\\Controllers\\'.ucfirst($this->controller) . 'Controller';
		if(!class_exists($controllerName))
		{
			$controllerName = 'Blissim\\Controllers\\DefaultController';
			$this->controller = 'default';
			$this->action = 'notfound';
			$this->params = array();
		}

		$controller = new $controllerName();
		$actionName= $this->action.'Action';
		if(!method_exists($controller, $actionName))
		{
			$actionName = 'notfoundAction';
			$this->action = 'notfound';
			$this->params = array();
		}
		//var_dump($controller,$actionName);

		ob_start();

		$controller->$actionName();
		$data = ob_get_clean();
		//si on a deja du contenu via la method
		if(!empty($data))
		{
			echo $data;
		}else{
			if($this->action!='notfound')
			$this->render();
		}

		$this->getFooter();
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function __get(string $name)
	{
		if(method_exists($this, 'get'.ucfirst($name)))
		{
			return $this->{'get'.ucfirst($name)}();
		}
		return $this->$name;
	}

	public function getAction()
	{
		return $this->action;
	}

	public function getController()
	{
		return $this->controller;
	}

	public function getParam(string $param)
	{
		if(isset($this->params[$param]))
		return $this->params[$param];

		return null;
	}

	public function setDb(PDO $db)
	{
		$this->db = $db;

	}

	public function getDb()
	{
		return $this->db;
	}
	protected function route()
	{
		$routingToExclude = str_replace($_SERVER['DOCUMENT_ROOT'],'',str_replace('\\', '/', realpath('.')));
		$routingToExplode = trim(str_replace($routingToExclude, '', $_SERVER['REQUEST_URI']),'/');
		$routingExploded = explode('/', $routingToExplode);

		//var_dump($routingExploded);

		$this->controller = (isset($routingExploded[0]) && (!empty($routingExploded[0]) || is_numeric($routingExploded))) ? array_shift($routingExploded) : 'default';

		$this->action = (isset($routingExploded[0]) && (!empty($routingExploded[0]) || is_numeric($routingExploded))) ? array_shift($routingExploded) : 'default';

		while(count($routingExploded) > 0)
		{
			
			if(count($routingExploded)%2===0){
				$this->params[array_shift($routingExploded)] = array_shift($routingExploded);
			}else{
				$this->params['id'] = array_shift($routingExploded);
			}
			
		}
		//var_dump($this->controller,$this->action,$this->params);


 
	}

	
	public function getFlash()
        {
            $flash = $_SESSION['toview']['flash'];
            unset($_SESSION['toview']['flash']);
            //$flash = $this->flash
            return $flash;
        }
        
        public function hasFlash()
        {
            return isset($_SESSION['toview']['flash']);
        }

        public function setFlash(string $value)
        {
            $_SESSION['toview']['flash'] = $value;//utf8_encode();
            //$this->flash = 
        }




	//


	public function setViewParams(string $param,$value)
	{
		$this->viewParams[$param]=$value;
	}

	public function setVars(string $key,$val)
	{
		$this->vars[$key]=$val;
	}

	public function abort(int $code = 404){

		switch ($code) {

			case 401:
				header('HTTP/1.0 401 Unauthorized');
				break;

			case 403:
				header('HTTP/1.0 403 Forbidden');
				break;

			default:
				header("HTTP/1.0 404 Not Found");
				break;
		}
		header('Location: /'.$code);

		exit();
	}


	protected function render()
	{
		if($this->hasFlash()){
			$this->setViewParams('flash_message',$this->getFlash());
		}
		extract($this->viewParams);

		require './views/'.$this->controller.'/'.$this->action.'.php';
	}

	private function flashMessage(){
		$flash_message = false;
		if($this->hasFlash()){
			$flash_message = $this->getFlash();
		}
		return $flash_message;
	}

	protected function getHeader(){
		$title = 'Blissim test';
		$flash_message = $this->flashMessage();
		require './views/_partials/header.php';
	}

	protected function getFooter(){

		require './views/_partials/footer.php';
	}
}
<?php
namespace Blissim\Controllers;

abstract class AbstractController {





	public function defaultAction()
	{
		// 
		$action = $this->getFc()->action;
		$controller = $this->getFc()->controller;
		$request = $this->getRequest();
		require './views/default.php';
	}

	public function notfoundAction()
	{
		// 
		$action = $this->getFc()->action;
		$controller = $this->getFc()->controller;
		//$request = $this->getRequest();
		header("HTTP/1.0 404 Not Found");
		require 'views/404.php';
	}

	public function getFc()
	{
		return FrontController::getInstance();
	}

	public function getRequest()
	{
		return $this->getFc()->getRequest();
	}

	

}
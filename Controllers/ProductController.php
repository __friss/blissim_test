<?php
namespace Blissim\Controllers;

use Blissim\Lib\Helpers;
use Blissim\Lib\Formchecker;
use Blissim\Models\Comment;

class ProductController extends AbstractController{

	

	public function getProducts($id=null){
		//fake cache
		if(!isset($_SESSION['products'])){
			$products = Helpers::callUrl('https://fakestoreapi.com/products');
			$_SESSION['products'] = $products;
		}else{
			$products = $_SESSION['products'];
		}

		if(!is_null($id)){
			$product = array_filter($products,function($v) use($id){ return $v['id'] == $id;});
			if(!empty($product)){

			 	return reset($product);
			}
		}

		return $products;
	}


	public function defaultAction()
	{
		$action = $this->getFc()->action;
		$controller = $this->getFc()->controller;
		$products = $this->getProducts();
		$this->getFc()->setViewParams('products',$products );

		require './views/product/index.php';
	}

	public function showcommentsAction(){
		$id = $this->getFc()->getParam('id');
		if(intval($id)<1){
			header('Location:/404');
			exit();
		}
		$product = $this->getProducts($id);
		$this->getFc()->setViewParams('product',$product);
		$this->getFc()->setViewParams('comments',Comment::product($id));
	}

	public function storecommentAction(){

		$this->getFc()->checkMandatory($_POST,array('csrf'));
		
		$id = $this->getFc()->getParam('id');
		if(intval($id)<1){
			header('Location:/404');
			exit();
		}

		$msg='';

		$formChecker = new Formchecker();//::get_instance();
		$formChecker->setAllowedReferer( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://'.$_SERVER['SERVER_NAME'].'/product/comment/'.$id);

		$success = false;
		if($formChecker->isClean())
		{
			$payload = Comment::validate($_POST);
			if(!isset($payload['errors'])){

				if(isset($payload['csrf'])){
					unset($payload['csrf']);
				}
				$payload['id_product'] = $id;
				$payload['created_at'] = date('Y-m-d H:i:s');
				$payload['updated_at'] = $payload['created_at'];

				if(Comment::create($payload)){
					$msg = 'Comment succesfullty added';
					$success = true;
					//Formchecker::clearCsrfToken();
				}else{
					$msg = 'Error, the comment has not been posted';
				}

			}else{
	            $msg ='An error occured<br>';
				$msg .= implode('<br>',$payload['errors']);
			}

		}else{
			$msg = $formChecker->getErrors();
		}
		$this->getFc()->setFlash($msg);

		if($success){
			header('Location: /product/');
		}else{
			header('Location: /product/comment/'.$id);
		}
		exit();
	}

	public function commentAction()
	{
		$id = $this->getFc()->getParam('id');
		if(intval($id)<1){
			header('Location:/404');
			exit();
		}
		$this->getFc()->setViewParams('id_product',$id);
	}
}
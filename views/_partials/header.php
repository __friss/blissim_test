<!DOCTYPE html>
<html lang="fr-FR" class="no-js"> 
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<title><?php echo $title; ?></title>
	<link rel='stylesheet' id='style-css'  href='../public/css/style.css' type='text/css' media='all' />

</head>

<body>
<?php

	if(isset($flash_message)):
		printf('<p>%s</p>',$flash_message);
	endif;

?>
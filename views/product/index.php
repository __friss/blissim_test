
<h1>Products</h1>
<?php

if(isset($products) && is_array($products) && !empty($products)): ?>

	<table>
		<tr>
			<th class="img">Image</th>
			<th>Tile</th>
			<th>Category</th>
			<th>Description</th>
			<th>Price</th>
			<th class="action">Action</th>
		</tr>

		<?php foreach ($products as $product): ?>
		<tr>
			<td>
				<figure>
					<img src="<?php echo $product['image'];?>" alt="<?php echo $product['title'];?>">
				</figure></td>
			<td><?php echo $product['title']; ?></td>
			<td><?php echo $product['category']; ?></td>
			<td><?php echo $product['description']; ?></td>
			<td><?php echo \Blissim\Lib\Helpers::uniformizePrice($product['price']);?>€</td>
			<td class="action"><a href="/product/comment/<?php echo $product['id'];?>">Add comment</a> <a href="/product/showcomments/<?php echo $product['id'];?>">View comments (<?php echo \Blissim\Models\Comment::count($product['id']);?>)</a></td>
		</tr>
		<?php endforeach;?>
	</table>
<?php
else:
	printf('<p>%s</p>','Sorry, there is no product yet...');

endif;
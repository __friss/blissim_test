<div>
	<?php if(isset($msg) && !empty($msg)):?>
		<p><?php echo $msg;?></p>
	<?php endif;?>

	<form action="/product/storecomment/<?php echo $id_product; ?>" method="post">
		<?php \Blissim\Lib\Formchecker::csrf();?>

		<?php include dirname(dirname(__FILE__)).'/_partials/comment/_form.php';?>

		<div>
			<input type="submit" value="send">
		</div>
	</form>


</div>

<a href="/product/showcomments/<?php echo $id_product;?>">Back to the Product#<?php echo $id_product;?></a>
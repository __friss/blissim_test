
<a href="/product/">Back to Product list</a> | <a href="/product/comment/<?php echo $product['id'];?>">Add comment</a>

<p><?php echo $product['title']; ?></p>
<p><?php echo $product['category']; ?></p>
<p><?php echo $product['description']; ?></p>
<p><?php echo \Blissim\Lib\Helpers::uniformizePrice($product['price']);?>€</p>


<?php

if(isset($comments) && is_array($comments) && !empty($comments)): ?>

	<table>
		<tr>
			<th>Nom</th>
			<th>Email</th>
			<th>Message</th>
			<th>Creation</th>
			<th>Modification</th>			
			<th class="action">Action</th>
		</tr>

		<?php foreach ($comments as $comment): ?>
		<tr>
		
			<td><?php echo $comment->name_comment; ?></td>
			<td><?php echo $comment->email_comment; ?></td>
			<td><?php echo $comment->text_comment; ?></td>
			<td><?php echo \Blissim\Lib\Helpers::convertDate(strtotime($comment->created_at)); ?></td>
			<td><?php echo \Blissim\Lib\Helpers::convertDate(strtotime($comment->updated_at)); ?></td>
			<td>
				<a href="/comment/edit/<?php echo $comment->id_comment;?>">Edit</a>
				<form name="delete" action="/comment/delete/<?php echo $comment->id_comment;?>" method="post">
					<?php \Blissim\Lib\Formchecker::csrf();?>

					<button type="submit" onclick="return confirm('Are you sure ?')">DELETE</button>
				</form>
				<!-- <a href="/comment/delete/<?php //echo $comment->id_comment;?>">Delete</a> -->
			</td>
		</tr>
		<?php endforeach;?>
	</table>
<?php
else:
	printf('<p>%s</p>','Sorry, there is no comment yet...');

endif;
?>
<a href="/product/">Back to Product list</a> | <a href="/product/comment/<?php echo $product['id'];?>">Add comment</a>
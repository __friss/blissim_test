-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 02 avr. 2021 à 22:08
-- Version du serveur :  5.7.22
-- Version de PHP :  7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blissim`
--

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id_comment` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_product` bigint(20) NOT NULL,
  `name_comment` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_comment`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id_comment`, `id_product`, `name_comment`, `email_comment`, `text_comment`, `created_at`, `updated_at`) VALUES
(2, 1, 'Pierre', 'pdesme@orange.fr', 'Bien!\r\nC\'est l\'été\r\n\r\nça va?222', '2021-04-01 22:32:24', '2021-04-02 19:52:31'),
(4, 2, 'zz', 'zz@zz.zz', 'zzzz', '2021-04-02 09:12:13', '2021-04-02 09:12:13'),
(5, 2, 'zz', 'zz@zz.zz', 'zzzz', '2021-04-02 09:15:20', '2021-04-02 09:15:20'),
(6, 2, 'zz', 'zz@zz.zz', 'zzzz', '2021-04-02 09:15:29', '2021-04-02 09:15:29'),
(7, 2, 'zz', 'zz@zz.zz', 'zzzz', '2021-04-02 09:15:39', '2021-04-02 09:15:39'),
(8, 2, 'zz', 'zz@zz.zz', 'zzzz3', '2021-04-02 09:17:19', '2021-04-02 09:17:19'),
(9, 2, 'zz', 'zz@zz.zz', 'zzzz3', '2021-04-02 09:17:30', '2021-04-02 09:17:30'),
(10, 2, 'zz', 'zz@zz.zz', 'zzzz3', '2021-04-02 09:18:18', '2021-04-02 09:18:18'),
(11, 2, 'zz', 'zz@zz.zz', 'zzzz3', '2021-04-02 09:18:34', '2021-04-02 09:18:34'),
(12, 2, 'zz', 'zz@zz.zz', 'zzzz34', '2021-04-02 09:19:29', '2021-04-02 09:19:29'),
(13, 2, 'zz', 'zz@zz.zz', 'zzzz34d', '2021-04-02 09:20:54', '2021-04-02 09:20:54'),
(14, 2, 'zz', 'zz@zz.zz', 'zzzz34dsv', '2021-04-02 09:22:10', '2021-04-02 09:22:10'),
(15, 3, 'zz', 'zz@zz.zz', 'zzzz', '2021-04-02 18:39:37', '2021-04-02 18:39:37'),
(18, 4, 'zzz', 'zzzzz@zzz.zz', 'zzdzedf', '2021-04-02 19:34:48', '2021-04-02 19:34:48'),
(19, 2, 'xxx', 'dd@dd.dd', 'sfb', '2021-04-02 19:36:24', '2021-04-02 19:36:24'),
(21, 2, 'qcssd', 'scs@sd.dd', 'sdcsdcs', '2021-04-02 19:39:00', '2021-04-02 19:39:00'),
(24, 1, 'Hervé', 'xx@xx.xx', 'xxx', '2021-04-02 19:54:06', '2021-04-02 19:54:06');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

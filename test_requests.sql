#Récupérer le prénom et le nom de famille de tous les clients qui ont commandé le produit avec l’ID 1;

SELECT lastname_client, firstname_client FROM clients WHERE id_client in (
	SELECT id_client FROM orders WHERE id_order IN (
		SELECT id_order FROM product_order WHERE id_product = 1
	)
)

#Récupérer tous les noms et quantités des produits vendus sur les 7 derniers jours.
SELECT p.name_product, SUM(DISTINCT po.quantity) as total FROM product_order po INNER JOIN products p ON p.id_product = po.id_product WHERE po.id_order IN(
		SELECT id_order FROM orders WHERE date(created_at) <= DATE(DATE_SUB(NOW(), INTERVAL 7 DAY))
	) GROUP BY po.id_product
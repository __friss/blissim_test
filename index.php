<?php

function BlissimAutoload($class)
{
	$file = str_replace('Blissim\\','',str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php');
	if(is_file($file)){
		require $file;
	}
	
}

spl_autoload_register('BlissimAutoload');

session_start();


$db = Blissim\Models\DBFactory::getPDOConnexion();
$frontController = Blissim\Controllers\FrontController::getInstance();
$frontController->setDb($db);
$frontController->run();

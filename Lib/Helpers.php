<?php
namespace Blissim\Lib;

class Helpers{

	public static function convertDate(int $ts, string $zone = 'Europe/Paris'){
        $date = new \DateTime();
        $date->setTimestamp($ts);
        $date->setTimezone(new \DateTimeZone($zone));
        return $date->format('d/m/Y H:i');
    }

public static function callUrl($url,$spoof=false,$response_code=false){
		$ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0';
		$timeout = 5;
		if(!function_exists('curl_init')){

				if($response_code){
					$headers = @get_headers($url);
					
	    			return (false!==$headers) ? substr($headers[0], 9, 3) : 0;
				}else{
					$args = array('http'=>array(
					'timeout' => $timeout,
					'method'=>"GET",
					'header'=> "Accept: */*",
					'ignore_errors' => false
					));
					if($spoof)
					{
						$args['user_agent'] = $ua;
					}
					$context = stream_context_create($args);

					$fp = fopen($url, 'r', false, $context);

					$data = stream_get_contents($fp);
					fclose($fp);
				}
				

		}else{
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, ($timeout*2));
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 

			if (preg_match('/^https:\/\//i', $url))
			{
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			}

			if($spoof)
			{
				curl_setopt($ch, CURLOPT_USERAGENT, $ua);
			}
			
			$data = curl_exec($ch);
			if($response_code && !curl_errno($ch))
			{
				return curl_getinfo($ch)['http_code'];
			
			}
			curl_close($ch);
		}
		if(strlen($data)>0){
			$data = json_decode($data,true);
			if(!is_null($data)) return $data;
		}
		return false;

	}

	

	public static function uniformizePrice($n)
	{
	    return number_format(str_replace(',','.',$n),2,'.','');
	}
}
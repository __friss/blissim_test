<?php
namespace Blissim\Lib;
class Request
{
	public function __construct()
	{
		return 'Request loaded';
	}

	public function isGet($value)
	{
		return (isset($_GET[$value])) ? $_GET[$value] : null;
	}
}
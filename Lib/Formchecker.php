<?php
namespace Blissim\Lib;

class Formchecker
{

	private static $_instance;
	//private $settings = array();

	private //$allowed = array('csrf','nom','prenom','email','phone'),
			//$valid = false,
			$checkReferer = true,
			$allowedReferer = array(),
			$errors = array(),
			$blockTime= 1,//minute
			$intervale = 1,//minute
			$nbAttempt = 20,// plus de 20essais dans une intervale de 1 minute = on block pendant 1 minute
			$method,
			$referer;

	const NOERROR ='Il\'y a pas d\'erreur';
	const BLOCKED = 'Vos tentatives ont été bloquées par raison de sécurité';
    const BRUTE = 'Vos tentatives ont été bloquées, trop d\'essais infructueux';
    const CSRF = 'Erreur de token';
    const REFERER = 'L\'url ne correspond pas';

	public static function get_instance()//
	{		
			if(is_null(self::$_instance))
			{			
				self::$_instance = new self();				
			}		

				
			return self::$_instance;		
	}

	public function __construct()
	 {
	     $this->init();
	 }

    // public function get($key)
    // {
    //     if (!isset($this->settings[$key])) {
    //         return null;
    //     }
    //     return $this->settings[$key];
    // }

	public function init()
	{
		//session_destroy();
//session_start();
		/*if (!isset($_SESSION) || session_id() == '' || session_status() == PHP_SESSION_NONE)
		{
    		session_start();
		}*/

//var_dump('init');
		
		$this->checkOutOfJail();

		

		$this->setIntervale();
		$this->setNumberAttempt();
		$this->setCsrfField();		
		$this->setReferer();

	//	var_dump($this->getReferer());

	}

	public function setAllowedReferer($ref)
	{

		$this->allowedReferer[] = $ref;
		

	}

	private function setIntervale()
	{
		if(!isset($_SESSION['intervale']))
		{
			$_SESSION['intervale'] = time() + 60*$this->intervale;
		}
	}

	public function getIntervale()
	{
		return $_SESSION['intervale'];
	}

	private function checkOutOfJail()
	{
		// Si on a dépassé le temps de blocage
		if(isset($_SESSION['nombre']) 
			&& $_SESSION['timestamp_limite'] < time())
		{
			//var_dump(time());
					//var_dump(date('d m Y H:i:s',time()));
					//var_dump($_SESSION['timestamp_limite']);

					//var_dump(date('d m Y H:i:s',$_SESSION['timestamp_limite']));


		                // Destruction des variables de session
			unset($_SESSION['nombre']);
			unset($_SESSION['timestamp_limite']);
			$this->unsetCookieBrute();
		}
		
	}

	public function isClean()
	{
		if(time() > $_SESSION['intervale'])
		{
			$_SESSION['nombre']++;
		}
		
		// Si le cookie n'existe pas 
		if(!$this->getCookieBrute())
		{			
			
			// Si on n'essaye pas de nous attaquer par force brute 
			if($this->getNumberAttempt() < $this->nbAttempt)
			{

				//

				if($this->isValidCsrf())
				{
					if(false === $this->checkReferer || $this->isValidReferer())
					{
						//là on peut commencer à traiter les data du form
						$this->errors=array();
						return true;
					}else{$this->errors['referer'] = self::REFERER;}
				}else{$this->errors['csrf'] = self::CSRF;}//csrf
			}else{
				$this->setCookieBrute();
				$this->errors['brute'] = self::BRUTE;
			}//nombre

		}else{$this->errors['blocked'] = self::BLOCKED;}//cookie

	}

	private function setNumberAttempt()
	{
		// Si la variable de session qui compte le nombre de soumissions n'existe pas
		if(!isset($_SESSION['nombre']))
		{
				// Initialisation de la variable 
			$_SESSION['nombre'] = 0;
	    		// Blocage pendant tant de min
			$_SESSION['timestamp_limite'] = time() + 60*$this->blockTime;
		}		

	}

	public function getNumberAttempt()
	{
		if(!isset($_SESSION['nombre']))
		{
			$this->setNumberAttempt();
		}
		//var_dump($_SESSION['nombre']);

		return $_SESSION['nombre'];
	}

	private function setCookieBrute()
	{
		if(!isset($_COOKIE['marqueur']))
		{
			$timestamp_marque = $_SESSION['timestamp_limite'];// time() + 60*$this->blockTime; // On le marque pendant une minute 
	        $cookie_vie = time() + 60*60*24; // Durée de vie de 24 heures pour le décalage horaire
	        
	        setcookie("marqueur", $timestamp_marque, $cookie_vie);
	        
	    }
	}

	private function unsetCookieBrute()
	{
		if(isset($_COOKIE['marqueur']))
		{
			setcookie("marqueur", '', time()-3600);
		}
	}

	public function getCookieBrute()
	{
		return isset($_COOKIE['marqueur']);
	}

	private function setMethod()
  	{
    	$this->method =  strtoupper($_SERVER['REQUEST_METHOD']);

  	}


	private function isValidCsrf()
	{
		//var_dump(isset($_POST['csrf']) && isset($_SESSION['csrf_token']));
		// var_dump($_POST['csrf']);
		// var_dump($_SESSION['csrf_token']);
		// var_dump($_POST['csrf']===$_SESSION['csrf_token']);
		return (isset($_POST['csrf']) && isset($_SESSION['csrf_token'])
			&& $_POST['csrf']===$_SESSION['csrf_token']
			&& ( (isset($_SESSION['csrf_token_time'])) ) 
			&& (time() - $_SESSION['csrf_token_time'] < 60*10)//10min
			);
	}

	private function setReferer()
	{
		if(is_null($this->referer))
		{
			if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!==null)
			{
				$this->referer = $_SERVER['HTTP_REFERER'];
			}else{
							//if(!isset($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER']===null)
							//{
				$page_url   = 'http';
				if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'){
					$page_url .= 's';
				}
				$ref = $page_url.'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

				//$ref = substr($ref,0,strpos('?',$ref)); // dangerreux peut être
					//if(!isset($_SESSION['OWN_REFERER']))
				//$_SESSION['OWN_REFERER'] = $ref;

				$this->referer = $ref;//$_SESSION['OWN_REFERER'];
			}
		}

if(false !== strpos($this->referer,'?'))
{
	$this->referer = substr($this->referer,0,strpos($this->referer,'?'));
}
		
	}

	public function isValidReferer()
	{
		return in_array($this->referer,$this->allowedReferer);
	}

	public function getReferer()
	{
		return $this->referer;
	}

	public function getErrors()
	{
		if(!empty($this->errors))
		{
			$out='';
			foreach($this->errors as $e)
			{
				$out.= $e.'<br>';
			}
			return $out;
		}

		return self::NOERROR;
	}

	public static function clearCsrfToken(){
		if(isset($_SESSION['csrf_token'])){
			unset($_SESSION['csrf_token']);
		}
	}

	private function setCsrfField()
	{
		//var_dump('$new = '.$new,'isset token? = '.isset($_SESSION['csrf_token']));
		if(! (isset($_SESSION['csrf_token']) && ( isset($_SESSION['csrf_token_time']) && (time() - $_SESSION['csrf_token_time'] < 60*10) )) )
		{
			//var_dump('token generation');
			$token = uniqid(mt_rand(), true);
	   		$_SESSION['csrf_token'] = hash('sha512',$token);
	   		$_SESSION['csrf_token_time'] = time();

	   		//var_dump('token = '.$_SESSION['csrf_token']);
	   	}
	}

	public function renderCsrfField()
	{
		return sprintf('<input type="hidden" id="csrf" name="csrf" value="%s">',$_SESSION['csrf_token']);
	}
	public static function csrf(){
		
		$f = self::get_instance();
		//$f->setCsrfField(true);
		echo $f->renderCsrfField();
	}
}
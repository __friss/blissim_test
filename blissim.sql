-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 01 avr. 2021 à 15:08
-- Version du serveur :  5.7.22
-- Version de PHP :  7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `blissim`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id_category` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_category` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_category` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_category` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  UNIQUE KEY `categories_slug_category_unique` (`slug_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id_client` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `title_client` enum('m','mme') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_client` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1_client` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2_client` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname_client` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname_client` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday_client` datetime DEFAULT NULL,
  `telephone_client` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insee_commune` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `newsletter_client` tinyint(1) NOT NULL DEFAULT '0',
  `terms_client` tinyint(1) NOT NULL DEFAULT '0',
  `isactive_client` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_client`),
  KEY `clients_id_user_foreign` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id_client`, `id_user`, `title_client`, `company_client`, `address1_client`, `address2_client`, `lastname_client`, `firstname_client`, `birthday_client`, `telephone_client`, `insee_commune`, `newsletter_client`, `terms_client`, `isactive_client`, `created_at`, `updated_at`) VALUES
(1, 1, 'm', NULL, '2 allée de Couspeau', NULL, 'Ferry', 'François-Régis', NULL, NULL, '26305', 0, 1, 1, '2021-03-31 21:09:53', '2021-03-31 21:09:53'),
(2, 2, 'm', NULL, '18 avenue saint lazare', 'le bois de laud', 'Bacchus', 'Edouard', NULL, NULL, '26198', 0, 1, 1, '2021-03-31 21:09:53', '2021-03-31 21:09:53');

-- --------------------------------------------------------

--
-- Structure de la table `communes`
--

DROP TABLE IF EXISTS `communes`;
CREATE TABLE IF NOT EXISTS `communes` (
  `insee_commune` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_commune` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomneutre_commune` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat_commune` decimal(10,7) NOT NULL,
  `lng_commune` decimal(10,7) NOT NULL,
  PRIMARY KEY (`insee_commune`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `insee_cp`
--

DROP TABLE IF EXISTS `insee_cp`;
CREATE TABLE IF NOT EXISTS `insee_cp` (
  `insee_commune` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`insee_commune`,`cp`),
  KEY `insee_cp_insee_commune_index` (`insee_commune`),
  KEY `insee_cp_cp_index` (`cp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id_order` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_client` bigint(20) UNSIGNED NOT NULL,
  `transaction_order` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_order`),
  KEY `orders_id_client_index` (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`id_order`, `id_client`, `transaction_order`, `created_at`, `updated_at`) VALUES
(1, 1, 'xxx', '2021-02-24 22:56:12', '2021-03-31 21:09:53'),
(2, 1, 'xxx22', '2021-03-29 01:56:12', '2021-03-31 21:09:53'),
(3, 1, 'xxx', '2021-01-29 02:56:12', '2021-03-31 21:09:53'),
(4, 1, 'xxx', '2021-03-30 01:56:12', '2021-03-31 21:09:53'),
(5, 1, 'xxx', '2021-03-27 02:56:12', '2021-03-31 21:09:53'),
(6, 2, 'xxx', '2021-03-28 23:56:12', '2021-03-31 21:09:53'),
(7, 2, 'xxx', '2021-03-24 02:56:12', '2021-03-31 21:09:53'),
(8, 2, 'xxx', '2021-03-25 02:56:12', '2021-03-31 21:09:53');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id_product` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_product` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_product` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_product` smallint(6) DEFAULT NULL,
  `description_product` text COLLATE utf8mb4_unicode_ci,
  `weight_product` tinyint(4) DEFAULT NULL,
  `isdigital_product` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id_product`, `name_product`, `reference_product`, `price_product`, `description_product`, `weight_product`, `isdigital_product`, `created_at`, `updated_at`) VALUES
(1, 'Box Printemps', 'bp0121', 30, NULL, 5, 0, '2021-03-31 21:09:53', '2021-03-31 21:09:53'),
(2, 'Box Eté', 'bp0221', 45, NULL, 5, 0, '2021-03-31 21:09:53', '2021-03-31 21:09:53'),
(3, 'Box Automne', 'bp0321', 40, NULL, 5, 0, '2021-03-31 21:09:53', '2021-03-31 21:09:53'),
(4, 'Box Hiver', 'bp0421', 55, NULL, 5, 0, '2021-03-31 21:09:53', '2021-03-31 21:09:53');

-- --------------------------------------------------------

--
-- Structure de la table `product_order`
--

DROP TABLE IF EXISTS `product_order`;
CREATE TABLE IF NOT EXISTS `product_order` (
  `id_product` bigint(20) UNSIGNED NOT NULL,
  `id_order` bigint(20) UNSIGNED NOT NULL,
  `quantity` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product`,`id_order`),
  KEY `product_order_id_product_index` (`id_product`),
  KEY `product_order_id_order_index` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_order`
--

INSERT INTO `product_order` (`id_product`, `id_order`, `quantity`) VALUES
(1, 1, 2),
(1, 3, 1),
(1, 6, 1),
(2, 4, 1),
(2, 5, 1),
(3, 2, 77),
(3, 7, 1),
(4, 3, 1),
(4, 8, 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_tag`
--

DROP TABLE IF EXISTS `product_tag`;
CREATE TABLE IF NOT EXISTS `product_tag` (
  `id_product` bigint(20) UNSIGNED NOT NULL,
  `id_tag` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_tag`,`id_product`),
  KEY `product_tag_id_product_index` (`id_product`),
  KEY `product_tag_id_tag_index` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id_tag` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_tag` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_tag` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_user`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'francoisregis.ferry@gmail.com', NULL, '$2y$10$tMGWiNi/CBfGrYA8AENFkejm.2EoUX2pp.oW/33o2Kxjauqoj9.76', NULL, '2021-03-31 21:09:53', '2021-03-31 21:09:53'),
(2, 'edouard.bacchus@gmail.com', NULL, '$2y$10$zsX5xyUcXKTMScIujnw.MuEn.p2vyWVFORHL8j1AjgUbhPC/w1Scu', NULL, '2021-03-31 21:09:53', '2021-03-31 21:09:53');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE;

--
-- Contraintes pour la table `insee_cp`
--
ALTER TABLE `insee_cp`
  ADD CONSTRAINT `insee_cp_insee_commune_foreign` FOREIGN KEY (`insee_commune`) REFERENCES `communes` (`insee_commune`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `product_order_id_order_foreign` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_order_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `products` (`id_product`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `product_tag`
--
ALTER TABLE `product_tag`
  ADD CONSTRAINT `product_tag_id_product_foreign` FOREIGN KEY (`id_product`) REFERENCES `products` (`id_product`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_tag_id_tag_foreign` FOREIGN KEY (`id_tag`) REFERENCES `tags` (`id_tag`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
